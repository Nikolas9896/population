<?php

namespace Figure;

class Square extends FigureAbstract
{
    public function Periment()
    {
        return $this->A * 4;
    }

    public function Side()
    {
        return $this->A * $this->A;
    }

    public static function AnySide($a)
    {
        return $a * $a;
    }

    public static function FactorSideA($a)
    {
        return self::FACTOR * $a;
    }
}