<?php
namespace Figure;

abstract class FigureAbstract
{

    protected $A;

    public function getA()
    {
        return $this->A;
    }

    public function setA($a)
    {
        $this->A = $a;
        return $this;
    }


    abstract public function Periment();

    abstract public function Side();
}