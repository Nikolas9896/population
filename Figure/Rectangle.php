<?php

namespace Figure;

class Rectangle extends FigureAbstract
{
    protected $B;

    public function getB()
    {
        return $this->B;

    }

    public function setB($b)
    {
        $this->B = $b;
        return $this;
    }

    public function Periment()
    {
        return $this->A * 2 + $this->B * 2;
    }

    public function Side()
    {
        return $this->A * $this->B;
    }

}


function getSide(Square $figure)
{
    try {
        return $figure->getB();
    } catch (Exception $e) {
        echo "Exception on execution";
    } catch (Error $e) {
        echo $e->getMessage();
        echo "\n";
        echo "Error on execution";
    } finally {
        echo "\n test \n";
    }
}