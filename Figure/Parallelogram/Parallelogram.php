<?php
namespace Figure\Parallelogram;

use Figure\Rectangle;

class Parallelogram extends Rectangle
{
    public $H;
    public function setH($h)
    {
        $this->H = $h;
        return $this;
    }
    public function getH()
    {
        return $this->H;
    }
    public function Side()
    {
        return $this->A * $this->H;
    }
}